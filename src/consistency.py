from src import csv_import

import seaborn as sns

sns.set()
sns.set_style("whitegrid")

train_bonds = csv_import.train_bonds
test_bonds = csv_import.test_bonds
test_charges = csv_import.test_charges
train_charges = csv_import.train_charges


def bond_type_to_pair(bond_type):
    # Input bond type and return last 3
    return bond_type[3:]


def bond_type_to_n(bond_type):
    return bond_type[0:3]


bond_types = train_bonds.bond_type.unique()
unique_pairs = sorted({bond_type_to_pair(b): 1 for b in bond_types}.keys())
print('Bond types: ', bond_types)
for (i, pair) in enumerate(unique_pairs):
    for bond_type in sorted([b for b in bond_types if pair == bond_type_to_pair(b)]):
        for (df, name) in [(train_bonds, 'train_errors')]:
            errors = df[(df.bond_type == bond_type) & (df.error == 1)]
            errors.to_csv('resources/train_bonds.csv', index=False)

