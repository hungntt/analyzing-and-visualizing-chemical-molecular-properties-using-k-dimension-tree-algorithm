import openbabel
import os
import pandas as pd
from tqdm import tqdm
from IPython import embed

path = '../resources/structures/'

# Initialize the OpenBabel object that we will use later.
obConversion = openbabel.OBConversion()
obConversion.SetInFormat("xyz")

# Define containers
mol_index = []  # This will be our dataframe index
bond_atom_0 = []
bond_atom_1 = []
bond_order = []
bond_length = []

for f in os.scandir(path):
    # Initialize an OBMol object
    mol = openbabel.OBMol()
    read_ok = obConversion.ReadFile(mol, f.path)
    if not read_ok:
        # There was an error reading the file
        raise Exception(f'Could not read file {f.path}')

    mol_name = f.name[:-4]
    mol_index.extend([mol_name] * mol.NumBonds())  # We need one entry per bond

    # Extract bond information
    mol_bonds = openbabel.OBMolBondIter(mol)  # iterate over all the bonds in the molecule
    for bond in mol_bonds:
        embed()
        bond_atom_0.append(bond.GetBeginAtomIdx() - 1)  # Must be 0-indexed
        bond_atom_1.append(bond.GetEndAtomIdx() - 1)
        bond_length.append(bond.GetLength())
        bond_order.append(bond.GetBondOrder())

df = pd.DataFrame({'molecule_name': mol_index,
                   'atom_index_0': bond_atom_0,
                   'atom_index_1': bond_atom_1,
                   'nbond': bond_order,
                   'L2Dist': bond_length})

df = df.sort_values(['molecule_name', 'atom_index_0']).reset_index(drop=True)

df.to_csv('resources/open_babel.csv', index=False)
print(df.head(10))
