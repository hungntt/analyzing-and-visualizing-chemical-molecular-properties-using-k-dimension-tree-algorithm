import networkx as nx
from scipy.spatial import cKDTree as KDTree
from tqdm.notebook import tqdm
import numpy as np
import pandas as pd
from src import csv_import
import logging
from IPython import embed

structures = csv_import.structures
train = csv_import.train
test = csv_import.test

VALENCE_MAX = {'C': 4, 'H': 1, 'N': 4, 'O': 2, 'F': 1}
VALENCE_STD = {'C': 4, 'H': 1, 'N': 3, 'O': 2, 'F': 1}

# expected distances in [A] for covalent 1 bond
BOND_DIST_C1 = {'C': 0.77, 'H': 0.38, 'N': 0.75, 'O': 0.73, 'F': 0.71}

# order used for finding bonds by atom type
BOND_ORDER = {'H': 0, 'F': 0, 'O': 1, 'N': 2, 'C': 3}


def add_bond(n_avail, n_bond, a0, a1, d1=None):
    key = tuple(sorted((a0, a1)))
    if key in n_bond:
        n_bond[key][0] += 1.0
    elif d1 is not None:
        n_bond[key] = [1.0, d1]
    else:
        raise Exception(f"{a0}, {a1} added after phase 1")

    n_avail[a0] -= 1
    n_avail[a1] -= 1


def get_bonded_atoms(atoms, n_bond, i):
    bonded = []
    for (a0, a1), (n, _) in n_bond.items():
        if a0 == i:
            bonded.append((a1, atoms[a1]))
        elif a1 == i:
            bonded.append((a0, atoms[a0]))

    bonded = sorted(bonded, key=lambda b: b[1])
    return "".join([b[1] for b in bonded]), [b[0] for b in bonded]


def search_bonds(kdt, coords, n_avail, n_bond, connected, is_leaf, atoms, a0, molecule, order, connect_once=True,
                 VALENCE=None):
    if VALENCE is None:
        VALENCE = VALENCE_STD

    atom0 = atoms[a0]  # a0 = 1, atoms[a0] = 'H', atoms = ['C', 'H', 'H', 'H', 'H']
    if n_avail[a0] == 0:
        return

    # select closest atoms ordered by distance: closest first
    # coords[a0] = array([ 0.00215042, -0.00603132,  0.00197612])
    # kdt.query(coords[a0], len(atoms))
    next_dist, next_i = kdt.query(coords[a0], min(1 + VALENCE[atom0], len(atoms)))
    next_dist = next_dist[1:]  # Remove itself a0
    next_i = next_i[1:]

    # For each valence closest atoms
    found = False

    for d1, a1 in zip(next_dist, next_i):
        if connect_once and a1 in connected[a0]:
            # connect_once = true. If true, return to the loop. Else continue next code
            continue

        atom1 = atoms[a1]  # C, a1 = 0
        predicted_bond = BOND_DIST_C1[atom0] + BOND_DIST_C1[atom1]
        # predicted_bond = 1.15 (C+H)
        # {'C': 0.77, 'H': 0.38, 'N': 0.75, 'O': 0.73, 'F': 0.71}

        if abs(d1 / predicted_bond) < 1.2:

            if n_avail[a1] > 0:  # n_avail of C > 0 ( = 4 for first time)
                add_bond(n_avail, n_bond, a0, a1, d1)
                connected[a0][a1] = 1
                connected[a1][a0] = 1
                if n_avail[a0] == 0 or n_avail[a1] == 0:
                    is_leaf[a0] = 1
                    is_leaf[a1] = 1
                found = True

        else:
            pass
    return found


def compute_bonds(structures, molecules):
    out_name = []
    out_a0 = []
    out_a1 = []
    out_n_bond = []
    out_dist = []
    out_error = []
    out_type = []

    cycle_name = []
    cycle_index = []
    cycle_seq = []
    cycle_atom_index = []

    charge_name = []
    charge_atom_index = []
    charge_value = []

    logging.basicConfig(filename='../log/compute_bonds.log', level=logging.INFO)

    for i_mol, name in tqdm(list(enumerate(molecules))):

        molecule = structures.loc[name]
        error = 0
        atoms = molecule.atom.values  # array(['C', 'H', 'H', 'H', 'H'], dtype=object)
        atoms_index = molecule.atom_index.values  # array([0, 1, 2, 3, 4], dtype=int64)

        n_avail = np.asarray([VALENCE_STD[a] for a in atoms])  # array([4, 1, 1, 1, 1])
        n_charge = np.zeros(len(atoms), dtype=np.float16)
        is_leaf = np.zeros(len(atoms), dtype=np.bool)  # is the atom in the leaves of connection tree?
        coords = molecule[['x', 'y', 'z']].values
        kdTree = KDTree(coords)  # use an optimized structure for closest match query, XYZ of its atoms, array([[
        # -1.26981359e-02,  1.08580416e+00,  8.00099580e-03],
        # [ 2.15041600e-03, -6.03131760e-03,  1.97612040e-03],
        # [ 1.01173084e+00,  1.46375116e+00,  2.76574800e-04],
        # [-5.40815069e-01,  1.44752661e+00, -8.76643715e-01],
        # [-5.23813635e-01,  1.43793264e+00,  9.06397294e-01]])
        n_bond = {}
        connected = {i: {} for i in atoms_index}  # {0: {}, 1: {}, 2: {}, 3: {}, 4: {}}

        # select Hydrogen first to avoid butadyne-like ordering failures (molecule_name=dsgdb9nsd_000023)
        ordered_atoms_index = list(atoms_index)  # [0, 1, 2, 3, 4]
        ordered_atoms_index.sort(key=lambda i: BOND_ORDER[atoms[i]])
        ordered_atoms_index = np.asarray(ordered_atoms_index)  # array([1, 2, 3, 4, 0], dtype=int64)

        # STEP 1
        for a0 in ordered_atoms_index:
            search_bonds(kdTree, coords, n_avail, n_bond, connected, is_leaf, atoms, a0, molecule, ordered_atoms_index,
                         connect_once=True,
                         VALENCE=VALENCE_STD)

        # STEP 2
        while ((n_avail > 0).sum() > 0 and is_leaf).sum() > 0:
            # HCN H and C is leaf
            progress = False
            for a0 in ordered_atoms_index:

                if n_avail[a0] > 0 and is_leaf[a0]:
                    for a1 in connected[a0]:
                        # C and N of HCN has n_avail = 2
                        if n_avail[a0] > 0 and n_avail[a1] > 0:
                            add_bond(n_avail, n_bond, a0, a1)
                            progress = True
                            if n_avail[a0] == 0 or n_avail[a1] == 0:
                                is_leaf[a0] = 1
                                is_leaf[a1] = 1

            if not progress:
                break

        # Gather remaining multiple bonds
        if n_avail.sum() > 0:
            # array(['O', 'C', 'C', 'N', 'H']), C2HNO Formyl cyanide, 000027
            for key in n_bond.keys():
                a0, a1 = key
                while n_avail[a0] > 0 and n_avail[a1] > 0:
                    add_bond(n_avail, n_bond, a0, a1)

        # STEP 3: ionized radicals
        if n_avail.sum() > 0:
            for i, a in zip(atoms_index, atoms):
                if a == 'N':
                    # NH3+
                    bonded_str, bonded_index = get_bonded_atoms(atoms, n_bond, i)
                    if bonded_str == 'HHH' and n_avail[i] == 0:
                        # dsgdb9nsd_000271

                        n_avail[i] += 1
                        n_charge[i] += 1
                        if search_bonds(kdTree, coords, n_avail, n_bond, connected, is_leaf, atoms, i, molecule,
                                        ordered_atoms_index,
                                        connect_once=False,
                                        VALENCE=VALENCE_MAX):
                            print(f"++ NH3+ found for {name} atom_index={i}")
                            logging.info(f"++ NH3+ found for {name} atom_index={i}")
                        else:
                            print(f"** NH3+ not found for {name} atom_index={i}")
                            logging.info(f"** NH3+ not found for {name} atom_index={i}")
                elif a == 'O' and n_avail[i] == 1:
                    # dsgdb9nsd_000271
                    # COO-
                    bonded_str, bonded_index = get_bonded_atoms(atoms, n_bond, i)
                    if bonded_str == 'C':
                        C_i = bonded_index[0]
                        C_bonded_str, C_bonded_index = get_bonded_atoms(atoms, n_bond, C_i)
                        if 'OO' in C_bonded_str:
                            has_2CO = False
                            for a1, i1 in zip(C_bonded_str, C_bonded_index):
                                key = tuple(sorted((C_i, i1)))
                                if a1 == 'O' and n_bond[key][0] == 2:
                                    has_2CO = True

                            if len(C_bonded_index) == 3 and has_2CO:
                                # Carboxyle
                                n_avail[i] -= 1
                                print(f"** COO- found for {name} C_atom_index={C_i}")
                                logging.info(f"** COO- found for {name} C_atom_index={C_i}")
                                for a1, i1 in zip(C_bonded_str, C_bonded_index):
                                    if a1 == 'O':
                                        n_charge[i1] = -0.5
                                        key = tuple(sorted((C_i, i1)))
                                        n_bond[key][0] = 1.5

        graph = nx.Graph([bond for bond in n_bond.keys()])

        unordered_cycles = nx.minimum_cycle_basis(graph)

        if len(unordered_cycles) > 0:
            for i_cycle, c in enumerate(unordered_cycles):
                available = {i: 1 for i in c}
                a0 = c[0]
                cycle = [a0]
                del (available[a0])  # Remove a0 from available
                for index in range(1, len(c)):

                    # get atoms bonded to a0
                    bonded = [b for b in n_bond.keys() if a0 in b]
                    bonded = list(map(lambda b: b[0] if b[1] == a0 else b[1], bonded))
                    embed()
                    # get next atom and remove it from cycle
                    assert (len(bonded) > 0)
                    found = False

                    for a1 in bonded:
                        if (a1 in bonded) and (a1 in available):
                            cycle.append(a1)
                            del (available[a1])
                            a0 = a1
                            found = True
                            break
                    assert found

                # Add cycles found to the cycle dataframe lists
                cycle_name.extend([name] * len(cycle))
                cycle_index.extend([i_cycle] * len(cycle))
                cycle_seq.extend(np.arange(len(cycle)))
                cycle_atom_index.extend(cycle)

            if n_avail.sum() > 0:
                error = 1
                print(f"Remaining bonding={n_avail.sum()} for molecule_name={name}, atoms: " +
                      ", ".join([f"{i}:{atoms[i]}" for i in atoms_index if n_avail[i] > 0]))
                logging.info(f"Remaining bonding={n_avail.sum()} for molecule_name={name}, atoms: " +
                             ", ".join([f"{i}:{atoms[i]}" for i in atoms_index if n_avail[i] > 0]))

            # Inputs for DataFrame bonds
            for (a0, a1), (n, dist) in n_bond.items():
                out_name.append(name)
                out_a0.append(a0)
                out_a1.append(a1)
                out_n_bond.append(n)
                out_dist.append(dist)
                out_error.append(error)
                out_type.append(f"{n:0.1f}" + "".join(sorted(f"{atoms[a0]}{atoms[a1]}")))

            # Inputs for DataFrame charges
            charge_name.extend([name] * len(atoms))
            charge_atom_index.extend(molecule.atom_index.values)
            charge_value.extend(n_charge)

    bonds = pd.DataFrame({
        'molecule_name': out_name,
        'atom_index_0': out_a0,
        'atom_index_1': out_a1,
        'n_bond': out_n_bond,
        'L2_dist': out_dist,
        'error': out_error,
        'bond_type': out_type
    })

    charges = pd.DataFrame({
        'molecule_name': charge_name,
        'atom_index': charge_atom_index,
        'charge': charge_value
    })

    cycles = pd.DataFrame({
        'molecule_name': cycle_name,
        'cycle_index': cycle_index,
        'cycle_seq': cycle_seq,
        'atom_index': cycle_atom_index
    })

    return bonds, charges, cycles


def main():
    train_bonds, train_charges, train_cycles = compute_bonds(structures.set_index('molecule_name'),
                                                             train.molecule_name.unique())
    test_bonds, test_charges, test_cycles = compute_bonds(structures.set_index('molecule_name'),
                                                          test.molecule_name.unique())
    train_bonds.to_csv('train_bonds.csv', index=False)
    train_charges.to_csv('train_charges.csv', index=False)
    train_cycles.to_csv('train_cycles.csv', index=False)
    test_bonds.to_csv('test_bonds.csv', index=False)
    test_charges.to_csv('test_charges.csv', index=False)
    test_cycles.to_csv('test_cycles.csv', index=False)


if __name__ == "__main__":
    main()
