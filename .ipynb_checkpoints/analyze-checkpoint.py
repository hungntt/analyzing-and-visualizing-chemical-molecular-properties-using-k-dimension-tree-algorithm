from src import csv_import
import matplotlib.pylab as plt

train = csv_import.train

train.groupby('type').count()['molecule_name'].sort_values().plot(kind='barh',
                                                                  color='grey',
                                                                  figsize=(15, 5),
                                                                  title='Count of Coupling Type in Train Set')
plt.show()
